import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Todo } from 'src/app/interfaces/Todo.interface';

@Component({
  selector: 'app-add-todo-form',
  templateUrl: './add-todo-form.component.html',
  styleUrls: ['./add-todo-form.component.css'],
})
export class AddTodoFormComponent implements OnInit {
  @Output() closeForm = new EventEmitter<boolean>();
  @Output() addTodoInForm = new EventEmitter<Todo>();
  todoForm: FormGroup;
  error: string = '';

  constructor(public formBuilder: FormBuilder) {
    this.todoForm = this.formBuilder.group({
      todo: [''],
    });
  }

  ngOnInit(): void {}

  closeFormInForm() {
    this.closeForm.emit();
  }

  onTodoChange() {
    this.error = '';
  }

  onSubmit() {
    const { todo } = this.todoForm.value;
    if (!todo) {
      this.error = 'Require';
    } else {
      const todoSend: Todo = {
        todo: todo,
        successfully: false,
      };
      this.todoForm = this.formBuilder.group({
        todo: [''],
      });
      this.addTodoInForm.emit(todoSend);
      this.closeForm.emit();
    }
  }
}
