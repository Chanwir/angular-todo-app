import { Component, OnInit } from '@angular/core';
import { Todo, UpdateTodo } from 'src/app/interfaces/Todo.interface';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css'],
})
export class TodoListComponent implements OnInit {
  TodoList: Todo[] = [
    { todo: 'Task1', successfully: false },
    { todo: 'Task2', successfully: true },
    { todo: 'Task3', successfully: true },
  ];

  isOpenAddForm: boolean = false;
  taskRemaining: number = 0;

  constructor() {
    this.updateTaskRemaining();
  }

  ngOnInit(): void {}

  updateTaskRemaining() {
    const countTaskRemaining = this.TodoList.reduce((acc, cur: Todo) => {
      return acc + (!cur.successfully ? 1 : 0);
    }, 0);
    this.taskRemaining = countTaskRemaining;
  }

  openAddForm() {
    this.isOpenAddForm = true;
  }

  closeAddForm(): void {
    this.isOpenAddForm = false;
  }

  addTodo(todo: Todo) {
    const newTodoList = [...this.TodoList];
    newTodoList.push(todo);
    this.TodoList = newTodoList;
    this.updateTaskRemaining();
  }

  updateTodo(obj: UpdateTodo) {
    const { todo, index } = obj;
    const newTodoList = [...this.TodoList];
    newTodoList[index] = todo;
    this.TodoList = newTodoList;
    this.updateTaskRemaining();
  }

  deleteTodo(index: number) {
    const newTodoList = [...this.TodoList];
    newTodoList.splice(index, 1);
    this.TodoList = newTodoList;
    this.updateTaskRemaining();
  }
}
