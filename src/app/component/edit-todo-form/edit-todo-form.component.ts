import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Todo } from 'src/app/interfaces/Todo.interface';
import { ListComponent } from '../list/list.component';

@Component({
  selector: 'app-edit-todo-form',
  templateUrl: './edit-todo-form.component.html',
  styleUrls: ['./edit-todo-form.component.css'],
})
export class EditTodoFormComponent implements OnInit {
  @Input() todo: Todo = { todo: '', successfully: false };

  editForm: FormGroup;
  error: string = '';

  constructor(private list: ListComponent, public formBuilder: FormBuilder) {
    this.editForm = this.formBuilder.group({
      todo: [''],
    });
  }

  ngOnInit(): void {
    this.editForm.setValue({
      todo: this.todo.todo,
    });
  }

  handleClickCloseEdit() {
    this.list.closeEdit();
  }

  handleChangeInput(value: string) {
    this.error = value ? '' : 'Todo List is require';
  }

  onSubmit() {
    const { todo } = this.editForm.value;
    if (!todo) {
      this.error = 'Todo List is require';
    } else {
      const todoSend: Todo = { ...this.todo, todo: todo };
      this.editForm = this.formBuilder.group({
        todo: [''],
      });
      this.list.updateTodo(todoSend);
      this.list.closeEdit();
    }
  }
}
