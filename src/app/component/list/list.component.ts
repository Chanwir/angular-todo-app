import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Todo, UpdateTodo } from 'src/app/interfaces/Todo.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  @Input() todo: Todo = { todo: '', successfully: false };
  @Input() index: number = -1;
  @Output() deleteTodoEvent = new EventEmitter<number>();
  @Output() updateTodoEvent = new EventEmitter<UpdateTodo>();

  onEdit: boolean = false;

  constructor() {}

  ngOnInit(): void {}

  deleteTodo() {
    this.deleteTodoEvent.emit(this.index);
  }

  updateTodo(newTodo: Todo) {
    this.updateTodoEvent.emit({ todo: newTodo, index: this.index });
  }

  toggleTodoSuccess() {
    const { todo, successfully } = this.todo;
    const newTodo: Todo = {
      todo: todo,
      successfully: !successfully,
    };
    this.updateTodoEvent.emit({ todo: newTodo, index: this.index });
  }

  handleClickEdit() {
    this.onEdit = true;
  }

  closeEdit() {
    this.onEdit = false;
  }
}
