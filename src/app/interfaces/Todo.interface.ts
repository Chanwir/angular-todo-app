export interface Todo {
  todo: string;
  successfully: boolean;
}

export interface UpdateTodo {
  todo: Todo;
  index: number;
}
