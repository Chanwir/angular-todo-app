import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TodoListComponent } from './component/todo-list/todo-list.component';
import { ListComponent } from './component/list/list.component';
import { AddTodoFormComponent } from './component/add-todo-form/add-todo-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditTodoFormComponent } from './component/edit-todo-form/edit-todo-form.component';

@NgModule({
  declarations: [
    AppComponent,
    TodoListComponent,
    ListComponent,
    AddTodoFormComponent,
    EditTodoFormComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, FormsModule, ReactiveFormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
